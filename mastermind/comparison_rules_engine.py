from .comparison_rules_names import ComparisonRulesNames

from .general_rules import *


class ComparisonRulesEngine(object):
    def __init__(self, requested_rule):
        self.requested_rule = requested_rule

    def select_rule_engine(self):
        if self.requested_rule == ComparisonRulesNames.EQUAL:
            # Return class
            return RuleEquals
