from datacollector.messageqclient.contact_message_queue import check_datastore_availability

from .net_generic_data import NetGenericData


class HostDiscoveryData(NetGenericData):
    def __init__(self, new_book):
        NetGenericData.__init__(self, new_book)

    def handle_new_hosts_up_list(self):
        pass
