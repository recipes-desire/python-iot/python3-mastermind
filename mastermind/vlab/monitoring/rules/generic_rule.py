import abc


class GenericRule(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, stored_data, collected_data):
        self.stored_data = stored_data
        self.collected_data = collected_data

    @abc.abstractmethod
    def apply_rule(self):
        raise Exception("Not Yet Implemented Exception")
