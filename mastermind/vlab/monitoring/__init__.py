from .host_discovery_data import HostDiscoveryData
from .port_scanning_data import PortScanningData

__all__ = ["HostDiscoveryData", "PortScanningData"]
