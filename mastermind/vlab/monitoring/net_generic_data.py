import abc


class NetGenericData(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def cross_validation_new_stored(self):
        raise Exception("Needs to be Implemented !")
