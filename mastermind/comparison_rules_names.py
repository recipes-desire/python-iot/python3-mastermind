from enum import Enum


class ComparisonRulesNames(Enum):
    EQUAL = 1
    NOT_EQUAL = 2
    LEFT_GREATER_THAN_RIGHT = 3
