from .generic_rule import GenericRule

#import logging
#logging.basicConfig(level=logging.DEBUG)
#logger = logging.getLogger(__name__)


class RuleEquals(GenericRule):
    def __init__(self, logger, collected_data, stored_data):
        GenericRule.__init__(self, logger, collected_data, stored_data)

    def same_size_check(self):
        size_equal = len(self.collected_data) == len(self.stored_data)
        return size_equal

    def apply(self):
        self.logger.debug(
            "Ready to Apply Binary Operation: Element-wise Equality Operation")

        if not self.same_size_check():
            # Different size datasets
            self.logger.warn(
                "Equality Test Failure: Not Equal Sizes ... Collected {%d} VS Stored {%d}",
                len(self.collected_data), len(self.stored_data))
            return False
        else:
            # Same sizes - Continue to remaining equality tests
            self.logger.debug(
                "Equality Test Success: Equal Sizes ... Collected {%d} VS Stored {%d} !",
                len(self.collected_data), len(self.stored_data))

        # *********************************************************
        # Same size datasets ... proceeding with content comparison
        # *********************************************************
        for i in range(0, len(self.collected_data)):
            dcollected = self.collected_data[i]

            is_found = False

            for j in range(0, len(self.stored_data)):
                dstored = self.stored_data[j]

                if dcollected == dstored:
                    # Collected is already stored
                    # Stop scanning Stored
                    is_found = True
                    break

            if not is_found:
                # Collected element not found in stored
                self.logger.warn(
                    "Collected Element Not Found in Redis-Datastore ... %s",
                    dcollected)
                return False

                return False

        return True
